from Qt6CustomWidgets import ToggleButtonAnimated
from Qt6CustomWidgets import ProgressBar
from Qt6CustomWidgets import GraphicWidget
from Qt6CustomWidgets import QtHandlers, DialogLogger, PlainTextEditHandler


def test_gridWidget():
    from PySide6.QtWidgets import QApplication, QLabel, QWidget, QGridLayout
    from Qt6CustomWidgets.PySide6.containers import GridWidget

    app = QApplication()
    w = QWidget()
    grid = GridWidget()
    for i in range(50):
        grid.addWidget(QLabel(f'label {i}'))
    layout = QGridLayout()
    layout.addWidget(grid)
    w.setLayout(layout)
    w.show()
    app.exec()


if __name__ == '__main__':
    test_gridWidget()