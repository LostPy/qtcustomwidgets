# QtCustomWidgets

My custom widgets created for PySide6.

## Global Informations

 * Author: LostPy
 * Date: 2021-04-18
 * Version: 1.1.1
 
## Requirement:
 * Python 3.7+
 * Qt6
 * PySide6

## Installation

To install the last version:
```
pip install git+https://gitlab.com/LostPy/QtCustomWidgets.git@PySide6
```

## Widgets List

|Category|Name|Version add|Functional|QtDesigner|
|--------|----|:---------:|:--------:|:--------:|
|Buttons|ToggleButtonAnimated|1.0.20210418|✅|✅|
|Graphics|GraphicWidget|1.0.20210418|✅|❌|
|Display|ProgressBar|1.0.20210418|✅|✅|
|Display|CircularProgressBar|1.0.20210425|❌|❌|
|Display|PlainTextEditHandler|1.0.20210429|✅|❌|
|Dialog|dialogLogger|1.0.20210429|✅|❌|
|Input|RangeSlider|1.1.20211101|✅|❌|
|Buttons|ColorButton|1.1.3|✅|❌|

## Layout list
|Name|Version add|Functional|QtDesigner|
|----|:---------:|:--------:|:--------:|
|FlowLayout|1.1.2|✅|❌|

## Import a Widget

To import a widget, you can use:
```py
from Qt6CustomWidgets.PySide6 import nameOfWidget
```

By example, to import the Progressbar:
```py
from Qt6CustomWidgets.PySide6 import Progressbar
```

## License

This package is under the MIT License

```
Copyright © 2022 LostPy <https://gitlab.com/LostPy>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
```
