"""RangeSlider

Originated from the PyQt5 version:
https://github.com/sleepbysleep/range_slider_for_Qt5_and_PyQt5/blob/master/pyqt5_ranger_slider/range_slider.py
"""
from PySide6.QtWidgets import QSlider, QStyle, QApplication, QStyleOptionSlider
from PySide6.QtCore import Slot, Signal, Property, QPoint, QRect, Qt
from PySide6.QtGui import QPalette, QPainter, QBrush, QPaintEvent, QPen


class RangeSlider(QSlider):
    """ A slider for ranges.
    
    This class provides a dual-slider for ranges, where there is a defined
    maximum and minimum, as is a normal slider, but instead of having a
    single slider value, there are 2 slider values.
    
    This class emits the same signals as the QSlider base class, with the 
    exception of valueChanged
    """
    valuesChanged = Signal(int, int)

    def __init__(self, *args, **kwargs):
        super(RangeSlider, self).__init__(*args, **kwargs)
        
        self._low = self.minimum()
        self._high = self.maximum()
        
        self.pressed_control = QStyle.SC_None
        self.setTickInterval(0)
        self.setTickPosition(QSlider.NoTicks)
        self.hover_control = QStyle.SC_None
        self.click_offset = 0
        
        # 0 for the low, 1 for the high, -1 for both
        self.active_slider = 0


    # ------------------------------
    # Qt Properties
    # ------------------------------

    @Property(int)
    def low(self) -> int:
        return self._low

    @low.setter
    def low(self, low: int):
        self._low = low
        self.update()

    @Property(int)
    def high(self) -> int:
        return self._high

    @high.setter
    def high(self, high):
        self._high = high
        self.update()

    # ------------------------------
    # Methods
    # ------------------------------

    def draw_groove(self, painter: QPainter, style):
        """draw groove"""
        opt = QStyleOptionSlider()
        self.initStyleOption(opt)
        opt.siderValue = 0
        opt.sliderPosition = 0
        opt.subControls = QStyle.SC_SliderGroove
        if self.tickPosition() != self.NoTicks:
            opt.subControls |= QStyle.SC_SliderTickmarks
        style.drawComplexControl(QStyle.CC_Slider, opt, painter, self)
        groove = style.subControlRect(QStyle.CC_Slider, opt, QStyle.SC_SliderGroove, self)
        return opt, groove

    def draw_span(self, painter: QPainter, style, opt, groove):
        """draw span"""
        self.initStyleOption(opt)
        opt.subControls = QStyle.SC_SliderGroove
        opt.siderValue = 0

        # first handle
        opt.sliderPosition = self._low
        low_rect = style.subControlRect(QStyle.CC_Slider, opt, QStyle.SC_SliderHandle, self)

        # second handle
        opt.sliderPosition = self._high
        high_rect = style.subControlRect(QStyle.CC_Slider, opt, QStyle.SC_SliderHandle, self)

        low_pos = self.__pick(low_rect.center())
        high_pos = self.__pick(high_rect.center())

        min_pos = min(low_pos, high_pos)
        max_pos = max(low_pos, high_pos)

        c = QRect(low_rect.center(), high_rect.center()).center()

        if opt.orientation == Qt.Horizontal:
            span_rect = QRect(QPoint(min_pos, c.y()-2), QPoint(max_pos, c.y()+1))
        else:
            span_rect = QRect(QPoint(c.x()-2, min_pos), QPoint(c.x()+1, max_pos))

        if opt.orientation == Qt.Horizontal:
            groove.adjust(0, 0, -1, 0)
        else:
            groove.adjust(0, 0, 0, -1)
        
        if self.isEnabled():
            highlight = self.palette().color(QPalette.Highlight)
            painter.setBrush(QBrush(highlight))
            painter.setPen(QPen(highlight, 0))

            painter.drawRect(span_rect.intersected(groove))

    def __pick(self, pt):
        if self.orientation() == Qt.Horizontal:
            return pt.x()
        else:
            return pt.y()
           
           
    def __pixelPosToRangeValue(self, pos):
        opt = QStyleOptionSlider()
        self.initStyleOption(opt)
        style = QApplication.style()
        
        gr = style.subControlRect(style.CC_Slider, opt, style.SC_SliderGroove, self)
        sr = style.subControlRect(style.CC_Slider, opt, style.SC_SliderHandle, self)
        
        if self.orientation() == Qt.Horizontal:
            slider_length = sr.width()
            slider_min = gr.x()
            slider_max = gr.right() - slider_length + 1
        else:
            slider_length = sr.height()
            slider_min = gr.y()
            slider_max = gr.bottom() - slider_length + 1
            
        return style.sliderValueFromPosition(self.minimum(), self.maximum(),
                                             pos-slider_min, slider_max-slider_min,
                                             opt.upsideDown)

    # ------------------------------
    # Events
    # ------------------------------

    def paintEvent(self, event):

        painter = QPainter(self)
        style = QApplication.style()

        # draw groove
        opt, groove = self.draw_groove(painter, style)

        # drawSpan
        self.draw_span(painter, style, opt, groove)

        for i, value in enumerate([self._low, self._high]):
            opt = QStyleOptionSlider()
            self.initStyleOption(opt)

            if i == 0:
                opt.subControls = QStyle.SC_SliderHandle
            else:
                opt.subControls = QStyle.SC_SliderHandle

            if self.tickPosition() != self.NoTicks:
                opt.subControls |= QStyle.SC_SliderTickmarks

            if self.pressed_control:
                opt.activeSubControls = self.pressed_control
            else:
                opt.activeSubControls = self.hover_control

            opt.sliderPosition = value
            opt.sliderValue = value                                  
            style.drawComplexControl(QStyle.CC_Slider, opt, painter, self)
            
    def mousePressEvent(self, event):
        event.accept()

        style = QApplication.style()
        button = event.button()
        
        # In a normal slider control, when the user clicks on a point in the 
        # slider's total range, but not on the slider part of the control the
        # control would jump the slider value to where the user clicked.
        # For this control, clicks which are not direct hits will slide both
        # slider parts
                
        if button:
            opt = QStyleOptionSlider()
            self.initStyleOption(opt)

            self.active_slider = -1
            
            for i, value in enumerate([self._low, self._high]):
                opt.sliderPosition = value                
                hit = style.hitTestComplexControl(style.CC_Slider, opt, event.position().toPoint(), self)
                if hit == style.SC_SliderHandle:
                    self.active_slider = i
                    self.pressed_control = hit
                    
                    self.triggerAction(self.SliderMove)
                    self.setRepeatAction(self.SliderNoAction)
                    self.setSliderDown(True)
                    break

            if self.active_slider < 0:
                self.pressed_control = QStyle.SC_SliderHandle
                self.click_offset = self.__pixelPosToRangeValue(self.__pick(event.position().toPoint()))
                self.triggerAction(self.SliderMove)
                self.setRepeatAction(self.SliderNoAction)
        else:
            event.ignore()
                                
    def mouseMoveEvent(self, event):
        if self.pressed_control != QStyle.SC_SliderHandle:
            event.ignore()
            return
        
        event.accept()
        new_pos = self.__pixelPosToRangeValue(self.__pick(event.position().toPoint()))
        opt = QStyleOptionSlider()
        self.initStyleOption(opt)
        
        if self.active_slider < 0:
            offset = new_pos - self.click_offset
            self._high += offset
            self._low += offset

            if self._low < self.minimum():
                diff = self.minimum() - self._low
                self._low += diff
                self._high += diff

            if self._high > self.maximum():
                diff = self.maximum() - self._high
                self._low += diff
                self._high += diff

        elif self.active_slider == 0:
            if new_pos >= self._high:
                new_pos = self._high - 1
            self._low = new_pos

        else:
            if new_pos <= self._low:
                new_pos = self._low + 1
            self._high = new_pos

        self.click_offset = new_pos

        self.update()

        self.valuesChanged.emit(self._low, self._high)


if __name__ == '__main__':
    import sys
    from PySide6.QtWidgets import QWidget, QLabel, QVBoxLayout, QHBoxLayout


    class Widget(QWidget):
        def __init__(self):
            super(Widget, self).__init__()
            layout = QVBoxLayout()
            self.label_min = QLabel("min value: XX", self)
            self.label_max = QLabel("maw value: XX", self)
            self.slider = RangeSlider(self)
            layout.addWidget(self.slider)
            display_layout = QHBoxLayout()
            display_layout.addWidget(self.label_min)
            display_layout.addWidget(self.label_max)
            layout.addLayout(display_layout)
            self.setLayout(layout)
            self.slider.valuesChanged.connect(self.on_slider_valuesChanged)

        @Slot(int, int)
        def on_slider_valuesChanged(self, low, high):
            self.label_min.setText(f"min value: {low}")
            self.label_max.setText(f"max value: {high}")


    app = QApplication(sys.argv)
    w = Widget()
    w.show()
    sys.exit(app.exec())
