"""A button to select a color.
"""
from PySide6.QtWidgets import QFrame, QColorDialog
from PySide6.QtCore import Qt, Slot, Signal, Property
from PySide6.QtGui import QColor, QPainter, QBrush


class ColorButton(QFrame):
    clicked = Signal()
    colorChanged = Signal(QColor)

    def __init__(self, parent=None, default_color: QColor = Qt.red):
        super().__init__(parent)
        self._color = QColor(default_color)
        self._pressed = False
        self.clicked.connect(self.changeColor)
        self.setCursor(Qt.PointingHandCursor)
        self.update()

    # ----------------------
    # Properties
    # ----------------------

    @Property(QColor)
    def color(self) -> QColor:
        return self._color

    @color.setter
    def color(self, new_color: QColor):
        self._color = QColor(new_color)
        self.colorChanged.emit(self._color)
        self.update()

    # ----------------------
    # Slots
    # ----------------------

    @Slot()
    def changeColor(self):
        color = QColorDialog.getColor(self._color, self.parent())
        if color.isValid():
            self.color = color

    # ----------------------
    # Events
    # ----------------------

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self._pressed = True

    def mouseReleaseEvent(self, event):
        if self._pressed:
            self._pressed = False
            self.clicked.emit()

    def paintEvent(self, event):
        super().paintEvent(event)
        painter = QPainter(self)
        if self.isEnabled():
            brush = QBrush(self._color)
        else:
            brush = QBrush(self._color.darker(120))
        painter.setBrush(brush)
        painter.drawRect(0, 0, self.width(), self.height())
        painter.end()


if __name__ == '__main__':
    import sys
    from PySide6.QtWidgets import QApplication, QWidget, QVBoxLayout
    from PySide6.QtCore import Qt

    class Widget(QWidget):
        def __init__(self):
            super().__init__()
            self.button = ColorButton(self)
            layout = QVBoxLayout()
            layout.addWidget(self.button)
            self.setLayout(layout)

    app = QApplication(sys.argv)
    w = Widget()
    w.setGeometry(100, 100, 600, 400)
    w.show()
    sys.exit(app.exec())
