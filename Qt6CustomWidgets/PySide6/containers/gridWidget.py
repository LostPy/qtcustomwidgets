from typing import Tuple, List, Union

from PySide6.QtWidgets import QWidget, QLayout, QScrollArea, QGridLayout
from PySide6.QtCore import Qt, QSize, Property
from PySide6.QtGui import QResizeEvent

from ..layouts import FlowLayout


class GridWidget(QScrollArea):
    """A subclass of QWidget to display many widget in a grid layout
    that adapts the number of columns to the width of the widget,
    or the number of rows to the height of the widget depending on the chosen orientation.
    """

    def __init__(self, parent=None, widgetsSize: QSize = QSize(100, 100)):
        super(GridWidget, self).__init__(parent)
        self.widgetsSize = widgetsSize
        self._widgetView = QWidget()
        self.flowLayout = FlowLayout()
        self._widgetView.setLayout(self.flowLayout)
        self.setWidget(self._widgetView)

    def setOrientation(self, orientation: Qt.Orientation):
        self.orientation = orientation

    @Property(QSize)
    def widgetsSize(self) -> QSize:
        return self._widgetsSize

    @widgetsSize.setter
    def widgetsSize(self, size: QSize):
        if not isinstance(size, QSize):
            raise ValueError(f"'widgetsSize' must be an instance of QSize, not an instance of '{type(size)}'")
        self._widgetsSize = size

    def addWidget(self, widget: QWidget):
        widget.setMinimumSize(self.widgetsSize)
        self.flowLayout.addWidget(widget)

    def addLayout(self, layout: QLayout):
        self.flowLayout.addLayout(layout)

    def takeAt(self, index: int):
        return self.flowLayout.takeAt(index)

