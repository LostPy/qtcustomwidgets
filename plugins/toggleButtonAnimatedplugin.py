from PySide6.QtDesigner import QDesignerCustomWidgetInterface
from PySide6.QtGui import QIcon
from PySide6.QtWidgets import QWidget

from Qt6CustomWidgets.PySide6 import ToggleButtonAnimated

from groups import GROUPS


class ToggleButtonAnimatedPlugin(QDesignerCustomWidgetInterface):

    def __init__(self, parent=None):
        super(ToggleButtonAnimatedPlugin, self).__init__(parent)
        self.initialized = False

    def initialize(self):
        if not self.initialized:
            self.initialized = True

    def isContainer(self) -> bool:
        return False

    def isInitialized(self) -> bool:
        return self.initialized

    def createWidget(self, parent) -> QWidget:
        return ToggleButtonAnimated(parent)

    def name(self) -> str:
        return 'ToggleButtonAnimated'

    def icon(self) -> QIcon:
        return QIcon()

    def group(self) -> str:
        return GROUPS['Buttons']

    def toolTip(self) -> str:
        return ""

    def whatThis(self) -> str:
        return ""

    def domXml(self) -> str:
        return f"""
            <ui language="c++">
                <widget class="{self.name()}" name="{self.name()}">
                    <property name="toolTip">
                        <string>{self.toolTip()}</string>
                    </property>
                    <property name="whatsThis">
                        <string>{self.whatsThis()}</string>
                    </property>
                </widget>
                <customwidgets>
                    <customwidget>
                        <class>{self.name()}</class>
                        <extends>QWidget</extends>
                    </customwidget>
                </customwidgets>
            </ui>"""

    def includeFile(self) -> str:
        return 'Qt6CustomWidgets.PySide6.buttons.toggleButtonAnimated'